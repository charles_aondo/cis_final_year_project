    <link rel="stylesheet" href="pageStylesheet.css">
    <div class="backImage">
        <?php
        /**
         * Created by PhpStorm.
         * User: aondo
         * Date: 2019-02-11
         * Time: 8:01 AM
         * Purpose:This page takes the booking id and deletes the information from the database
         */
        //    Connecting to the database
        @$myDb = new mysqli('localhost', 'root', '', 'saloon101');

        //Checking to see if there is a connection to the database
        if(mysqli_connect_error()){
            echo "Could not connect to the database";
            header("location:../index.php");
            die("</body></html>");
        }
        //    Getting the  booking ID from the url to store it into the database
        $urlBookingID = $_GET['bookingID'];
        if(empty($urlBookingID)){
            header("location:cancelBooking.php");
            echo "Data not passed";
            die();
        }else {
            //Stripping away the any sql malicious code before inserting into the database
            mysqli_real_escape_string($myDb, $urlBookingID);

            $query1 = 'DELETE  FROM customers WHERE bookingID = "' . $urlBookingID . '" ';
            $query2 = 'DELETE FROM booking WHERE bookingID = "' . $urlBookingID . '"';

            $editResult = $myDb->query($query1);
            $editResult2 = $myDb->query($query2);

            if ($editResult && $editResult2 == true) {
                ?>
                <?php
                echo "
            <script type=\"text/javascript\">
                var askUser = confirm(\"Your Order has been Cancelled!! \");
                if(askUser == 1){
                    window.location.replace(\"../index.php\");
                }else {
                    window.location.replace(\"../index.php\");
                }
            </script>
        ";

            }
        }
        $myDb->close();
        ?>
    </div>

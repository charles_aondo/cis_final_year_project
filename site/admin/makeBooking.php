<!--
Author:Charles Aondo
Date:2019-02-1
Purpose:This page is use to accept bookings from the various users and post the data to the database
-->

<?php
    //Funtion that validates any required field
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    $phoneError = "";
    $serviceTYpeError = "";
    $dateError = "";
    $timeError = "";
    //Testing
    $databaseError = "";

    if(isset($_POST['submit'])){
        //            Connecting to the database
        @$myDb = new mysqli('localhost', 'root', '', 'saloon101');
        //Redirect the user to the home page if database connection fails
        if(mysqli_connect_error()){
            echo "Could not connect to the database";
            header("location:../index.php");
            die("</body></html>");
        }
        $validatePhoneErr = false;
        $validatesServiceErr = false;
        $schduleTriigger = false;
        $schduleTriigger2 = false;
        //Getting length of the phone number for validation
        $numlength = strlen((string)$_POST["phoneNumber"]);

        if(!is_numeric($_POST['phoneNumber'])){
            $phoneError = "Field must be a number";
        }elseif ($numlength != 10){
            $phoneError = "Phone number must be 10 digits long";
        }else{
            //Collect data after passing validation
           $phoneNumberCollected = mysqli_real_escape_string($myDb,$_POST['phoneNumber']);
           $validatePhoneErr = true;
        }
        //Validating service type selection
        if($_POST['serviceType'] == 0){
            $serviceTYpeError = "Please Select a Service";
        }else{
            $validatesServiceErr = true;
            $serviceTypeCollected = mysqli_real_escape_string($myDb,$_POST['serviceType']);
        }
        if(($_POST['time'] == 0) ){
            $timeError = "Please pick a time";
        }else{
            $schduleTriigger = true;
            $timeCollected = $_POST['time'];

        }if($_POST['date'] ==0){
            $dateError= "Please pick a date";
        }else{
            $schduleTriigger2 = true;
            $datCollected = $_POST['date'];
        }
        //Collecting the rest of the data
        if($validatesServiceErr == true && $validatePhoneErr == true && $schduleTriigger == true && $schduleTriigger2 == true){
            //Collect the rest of the data after the pass the html required field and validate for sql injection
            $firstNameCollected = mysqli_real_escape_string($myDb,$_POST['firstName']);
            $lastNameCollected = mysqli_real_escape_string($myDb,$_POST['lastName']);
            $emailAddressCollected = mysqli_real_escape_string($myDb,$_POST['inputEmail']);
            $serviceProviderCollected = mysqli_real_escape_string($myDb,$_POST['serviceProvider']);

            //Getting random numbers and letters from users first and last name to generate a booking ID
            $firstLetterName = substr($firstNameCollected,-7,1);
            $secondletterNmae = substr($lastNameCollected,-7,1);
            $bookingNumber = random_int(1000,9999);

            $bookingID = $bookingNumber.$firstLetterName.$secondletterNmae;

            $stmt = $myDb->prepare("INSERT INTO customers(bookingID,phoneNumber,firstName,lastName,emailAddress) VALUES (?,?,?,?,?)");
            $stmt->bind_param("sssss",$bookingID,$phoneNumberCollected,$firstNameCollected,$lastNameCollected,$emailAddressCollected);
//            $stmt->execute();
            $stmt1 = $myDb->prepare("INSERT INTO booking(bookingID,serviceProvider,serviceType,date,time,phoneNumber) VALUES (?,?,?,?,?,?)");
            $stmt1->bind_param("ssssss",$bookingID,$serviceProviderCollected,$serviceTypeCollected,$datCollected,$timeCollected,$phoneNumberCollected);
            $stmt->execute();
            $stmt1->execute();
            //A pop that tells the customer their booking is completed

            if($stmt->affected_rows && $stmt->affected_rows == 1){

                //Below is a javascript that informs the user about a successful booking and redirects them to the homepage
                echo "
            <script type=\"text/javascript\">
                var askUser = confirm(\"Thank you for your booking!! \\nAn email has been sent to you!! Click ok to the homepage\");
                if(askUser == 1){
                    window.location.replace(\"../index.php\");
                }else {
                    window.location.replace(\"../index.php\");
                }
            </script>
        ";
            }
        }
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Roberto's Fresh Cuts Appointment Booking</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bookingPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body id="orderPageBackGroundColor">
    <div class="container">
        <h2>Please Enter Your Booking Details</h2>
        <span class="warning"><?php echo $databaseError;?></span>
        <form method="post" action="makeBooking.php">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>First Name</label>
                    <input type="text" name="firstName" class="form-control" required >
                </div>
                <div class="form-group col-md-6">
                    <label>Last Name</label>
                    <input type="text" name="lastName" class="form-control" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Email</label>
                    <input type="email" class="form-control" name="inputEmail" placeholder="someone@email.com" required>
                </div>
                <div class="form-group col-md-6">
                    <label>Phone Number</label>
                    <input type="tel" class="form-control" placeholder="9029781306" name="phoneNumber" required>
                    <span class="warning"><?php echo $phoneError;?></span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputCity">Service Provider</label>
                    <select name="serviceProvider" class="form-control" required>
                        <option value="Roberto">Roberto</option>
                        <option  selected>Any</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputState">Service Type</label>
                    <select name="serviceType" class="form-control" required>
                        <option value="0" selected >Select Service...</option>
                        <option value="1">Black Max(1 hr)</option>
                        <option value="2">Fades' Hair cut and Beard (30)</option>
                        <option value="3">Men's Regular Haircut (30)</option>
                        <option value="4">Kids Cuts under 10yrs (30)</option>
                        <option value="5">Boys cut - fade - Hair Tatto (30)</option>
                        <option value="6">Women's Hair Cuts (30)</option>
                        <option value="7">Line up + Beard Trim(30)</option>
                    </select>
                    <span class="warning"><?php echo $serviceTYpeError;?></span>
                </div>
            </div>
            <h2>Please Select your Availability</h2>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputState">Date</label>
                    <select name="date" class="form-control" required>
                        <option value="0" selected >Pick a Date...</option>
                        <option value="2019-02-15">Monday:feb,15</option>
                        <option value="2019-02-16">Tuesday:feb,16</option>
                        <option value="2019-02-17">Wednesday:feb,17</option>
                        <option value="2019-02-18">Thursday:feb,18</option>
                        <option value="2019-02-19">Friday:feb,19</option>
                        <option value="2019-02-20">Saturday:feb,20</option>
                    </select>
                    <span class="warning"><?php echo $dateError;?></span>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label>Time</label>
                <select name="time" class="form-control">
                    <option selected value="0" >Select</option>
                    <option value="9:00 AM">9:00 AM</option>
                    <option value="9:30 AM">9:30 AM</option>
                    <option value="10:00 AM">10:00 AM</option>
                    <option value="10:30 AM">10:30 AM</option>
                    <option >11:00 AM</option>
                    <option >11:30 AM</option>
                    <option >12:00 AM</option>
                    <option >12:30 PM</option>
                    <option >1:000 PM</option>
                    <option >1:30 PM</option>
                    <option >2:00 PM</option>
                    <option >2:30 PM</option>
                    <option >3:30 PM</option>
                    <option >4:00 PM</option>
                    <option >4:30 PM</option>
                    <option >5:30 PM</option>
                    <option >6:30 PM</option>
                </select>
                <span class="warning"><?php echo $timeError;?></span>
                </div>
            <button type="submit" class="btn btn-primary" name="submit">Submit Booking</button>
        </form>
    </div>
</body>
</html>


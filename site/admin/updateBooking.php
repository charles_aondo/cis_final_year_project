<!--
Author:Charles Aondo
Date:2019-02-12
Purpose:This page is use to update the users booking with information loaded  from the database
-->
<?php
//Database Connection
    $databaseError ="";
    @$myDb = new mysqli('localhost', 'root', '', 'saloon101');
    $urlBookingID;
    if(mysqli_connect_error()){
        $databaseError="Bad Connection";
    }
//Directing the user to the search page if the do get here without a booking ID number
    if(empty($_GET['bookingID'])){
        $databaseError="Booking id not found";
        header("location:cancelBooking.php");
        die();
    }else{
        $urlBookingID= $_GET['bookingID'];
        mysqli_real_escape_string($myDb,$urlBookingID);
        //https://stackoverflow.com/questions/27600017/delete-a-same-data-from-multiple-table-in-php
        $query ="SELECT a.*, b.* FROM customers a
                  INNER JOIN booking b ON a.bookingID = b.bookingID
                  WHERE a.bookingID='$urlBookingID'";
              $result = $myDb->query($query);
        $row= $result->fetch_assoc();
        //Putting the result collected in variables
        $firstName =$row['firstName'];
        $lastName = $row['lastName'];
        $phoneNumber = $row['phoneNumber'];
        $emailAddress = $row['emailAddress'];
    }
?>
<!--
Name:Charles Aondo
Date:2019-02-13
Purpose:This is the form that prefills the user with their information to be updated from the database
...-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Your Vegetables</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bookingPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body id="orderPageBackGroundColor">
    <div class="container">
        <h2>Update Your Booking Details</h2>
        <h4 class="warning">Only Fields Marked * Can be edited</h4>
        <form method="post" action="executeUpdatePage.php">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>First Name</label>
                    <input value="<?php echo $firstName?>" type="text" name="firstName" class="form-control" disabled>
                    <input value="<?php echo $urlBookingID?>" type="hidden" name="urlBookingID" class="form-control" >
                </div>
                <div class="form-group col-md-6">
                    <label>Last Name</label>
                    <input value="<?php echo $lastName?>" type="text" name="lastName" class="form-control" disabled>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Email</label>
                    <input type="email" class="form-control" name="inputEmail" value="<?php echo $emailAddress?>" disabled >
                </div>
                <div class="form-group col-md-6">
                    <label>Phone Number</label>
                    <input type="tel" class="form-control" name="phoneNumber" value="<?php echo $phoneNumber?>" disabled>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputCity">Service Provider*</label>
                    <select name="serviceProvider" class="form-control">
                        <option value="Roberto">Roberto</option>
                        <option  selected>Any</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputState">Service Type*</label>
                    <select name="serviceType" class="form-control" required>
                        <option value="0" selected>Select......</option>
                        <option value="1">Black Max(1 hr)</option>
                        <option value="2">Fades' Hair cut and Beard (30)</option>
                        <option value="3">Men's Regular Haircut (30)</option>
                        <option value="4">Kids Cuts under 10yrs (30)</option>
                        <option value="5">Boys cut - fade - Hair Tatto (30)</option>
                        <option value="6">Women's Hair Cuts (30)</option>
                        <option value="7">Line up + Beard Trim(30)</option>
                    </select>
                </div>
            </div>
            <h2>Please update your Availability</h2>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputState">Date*</label>
                    <select name="date" class="form-control" required>
                        <option value="0">Select......</option>
                        <option value="2019-02-15">Monday:feb,15</option>
                        <option value="2019-02-16">Tuesday:feb,16</option>
                        <option value="2019-02-17">Wednesday:feb,17</option>
                        <option value="2019-02-18">Thursday:feb,18</option>
                        <option value="2019-02-19">Friday:feb,19</option>
                        <option value="2019-02-20">Saturday:feb,20</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label>Time*</label>
                <select name="time" class="form-control">
                    <option selected value="0" >Select....</option>
                    <option value="9:00 AM">9:00 AM</option>
                    <option value="9:30 AM">9:30 AM</option>
                    <option value="10:00 AM">10:00 AM</option>
                    <option value="10:30 AM">10:30 AM</option>
                    <option >11:00 AM</option>
                    <option >11:30 AM</option>
                    <option >12:00 AM</option>
                    <option >12:30 PM</option>
                    <option >1:000 PM</option>
                    <option >1:30 PM</option>
                    <option >2:00 PM</option>
                    <option >2:30 PM</option>
                    <option >3:30 PM</option>
                    <option >4:00 PM</option>
                    <option >4:30 PM</option>
                    <option >5:30 PM</option>
                    <option >6:30 PM</option>
                </select>
                </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-primary" name="submit">Update Booking</button>
                </div>
                <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-primary" name="homeButton">Home</button>
                </div>
            </div>
        </form>
    </div>
</body>
</html>


<!--
Author:Charles Aondo
Date:2019-02-11
Purpose:This page allows a user to cancel their bookings
-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cancel Your Booking</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bookingPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body class="backImage">
    <div class="container">
        <?php
        //Redirecting the user to the home page when the click on home
        if(isset($_POST["homeButton"])){
            header("location:../index.php");
            die("</body></html>");
        }
        $databaseError ="";
        $phoneError="";
        $editResult = "";
        $row;
        if(isset($_POST['submit'])){
            //            Connecting to the database
            @$myDb = new mysqli('localhost', 'root', '', 'saloon101');
            //Redirect the user to the home page if database connection fails
            if(mysqli_connect_error()){
                echo "Could not connect to the database";
                header("location:../index.php");
                die("</body></html>");
            }
            //Getting length of the phone number for validation
            $numlength = strlen((string)$_POST["phoneNumber"]);
            if(empty($_POST['bookingID'] || $_POST['phoneNumber']) ){
                $phoneError ="Empty fields";
            }elseif(!is_numeric($_POST['phoneNumber'])){
                $phoneError = "Phone number must be a number";
            }elseif ($numlength != 10){
                $phoneError = "Phone number must be 10 digits long";
            }else{
                //Collect data after passing validation
                $phoneNumberCollected = mysqli_real_escape_string($myDb,$_POST['phoneNumber']);
                $bookingIDCollected = mysqli_real_escape_string($myDb,$_POST['bookingID']);
                $query = 'SELECT * FROM customers WHERE bookingID = "'.$bookingIDCollected.'" or  phoneNumber = "'.$phoneNumberCollected.'"';
                $editResult = $myDb->query($query);

                if($editResult->num_rows < 1){
                    $phoneError = "Invalid Credentials!!";
                }
                if($editResult->num_rows > 0){
                    echo '<table class="table table-bordered table-striped">';
                    echo '<tr>';
                    echo '<th>Booking ID</th>';
                    echo '<th>Phone Number</th>';
                    echo '<th>First Name</th>';
                    echo '<th>Last Name</th>';
                    echo '<th>Email Address</th>';
                    echo '<th>Click X</th>';
                    echo '<th>Update  </th>';
                    $row = $editResult->fetch_assoc();

                    echo '</tr>';
                    echo '<td>'.$row['bookingID'].'</td>';
                    echo '<td>'.$row['phoneNumber'].'</td>';
                    echo '<td>'.$row['firstName'].'</td>';
                    echo '<td>'.$row['lastName'].'</td>';
                    echo '<td>'.$row['emailAddress'].'</td>';
                    echo '<td> 
                               <a href="delete.php?bookingID='.$row['bookingID'].'" >Delete</a>
                                </td>';
//                    }
                    echo '<td> 
                               <a href="updateBooking.php?bookingID='.$row['bookingID'].'" >Update</a>
                                </td>';
//                    }
                    echo '</table>';
                }
            }
        }

        ?>

        <h2>Please Enter Your Booking Credentials</h2>
        <form method="post" action="cancelBooking.php">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Booking ID</label>
                    <input type="text" name="bookingID" class="form-control" >
                </div>
                <div class="form-group col-md-6">
                    <label>Phone Number</label>
                    <input type="text" name="phoneNumber" class="form-control">
                </div>
                <span class="warning"><?php echo $phoneError;?></span>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-primary" name="submit">Search Booking</button>
                </div>
                <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-primary" name="homeButton">Home</button>
                </div>
            </div>
        </form>

    </div>
</body>
</html>

